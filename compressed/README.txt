# DEX-NET 2.0 Adv-Phys DATASET

# OVERVIEW
This is the dataset of random antiopdal grasps used as a baseline in the Dex-Net 2.0 paper.
The dataset contains 400 grasps collected on an ABB YuMi with the 8 adversarial training objects.
For more information please see the project website at berkeleyautomation.github.io/dex-net.

If you use this dataset in a publication, please cite:

Jeffrey Mahler, Jacky Liang, Sherdil Niyaz, Michael Laskey, Richard Doan, Xinyu Liu, Juan Aparicio Ojea,
and Ken Goldberg. "Dex-Net 2.0: Deep Learning to Plan Robust Grasps with Synthetic Point Clouds and Analytic
Grasp Metrics." Robotics, Science, and Systems, 2017. Cambridge, MA.

# DATA FORMAT
Files are in compressed numpy (.npz) format and organized by attribute.
There are 400 unique datapoints, each with three relevant attributes:

  1) depth_ims_tf_table:
       Description: depth images transformed to align the grasp center with the image center and the grasp axis with the middle row of pixels
       File dimension: 400x32x32x1
       Organization: {num_datapoints} x {image_height} x {image_width} x {num_channels}
       Notes: Rendered with OSMesa using the parameters of a Primesense Carmine 1.08

  2) hand_poses:
       Description: configuration of the robot gripper corresponding to the grasp
       File dimension: 400x6
       Organization: {num_datapoints} x {hand_configuration}, where the only relevant column is:
         2: depth, in meters, of gripper center from the camera that took the corresponding depth image
	 The gripper width was 5cm, corresponding to the width of our custom ABB YuMi grippers

  3) labels:
       Description: 1 if the grasp was able to hold the object while lifting, transporting, and shaking the object and 0 otherwise
       File dimension: 400
       Organization: {num_datapoints}et 2.0

# DATA INDEXING
The index into the first dimension of each file corresponds to a different datapoint.
Furthermore, the same index across files with the same number corresponds to the same datapoint.

For example, datapoint 78 could be accessed in Python as:
  depth_im = np.load('depth_ims_tf_table_00000.npz')['arr_0'][78,...]
  hand_pose = np.load(hand_poses_00000.npz')['arr_0'][78,...]
  label = np.load(labels_00000.npz')['arr_0'][78,...]
