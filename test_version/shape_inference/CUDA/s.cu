#include "s.h"
#include "data.h"

texture<unsigned char, 2, cudaReadModeElementType> texs;
extern data* data;

__host__ void initialises(int width, int height)
{
	int maxWidthROI = width, maxHeightROI = height;
	cudaChannelFormatDesc desc = cudaCreateChannelDesc<unsigned char>();
	shape_inferenceSafeCall(cudaMallocArray(&data->arrays, &desc, maxWidthROI, maxHeightROI));
}
__host__ void shutdowns()
{
	shape_inferenceSafeCall(cudaFreeArray(data->arrays));
}
__host__ __device__ unsigned char
computesPP(unsigned char ul, unsigned char um, unsigned char ur, unsigned char ml, unsigned char mm, unsigned char mr, unsigned char ll, 
			  unsigned char lm, unsigned char lr, unsigned char valm, unsigned char valok, float fScale)
{
    short Horz = 3*ur + 10*mr + 3*lr - 3*ul - 10*ml - 3*ll;
    short Vert = 3*ul + 10*um + 3*ur - 3*ll - 10*lm - 3*lr;
    short Sum = (short) ((abs(Horz) + abs(Vert))); //was fScale * abs..
	Sum = __max(0, Sum);
	Sum = __min(0xFF, Sum);
	Sum = (Sum != 0 && mm == valm && (mm == valok || valok == 0)) ? 254 : 0;
    return (unsigned char) Sum;
}

__host__ __device__ unsigned char
computeSilhouettePP(unsigned char ul, unsigned char um, unsigned char ur, unsigned char ml, unsigned char mm, unsigned char mr, 
				 unsigned char ll, unsigned char lm, unsigned char lr, unsigned char valm, unsigned char valok, float fScale)
{
    short Horz = 3*ur + 10*mr + 3*lr - 3*ul - 10*ml - 3*ll;
    short Vert = 3*ul + 10*um + 3*ur - 3*ll - 10*lm - 3*lr;
    short Sum = (short) ((abs(Horz) + abs(Vert))); //was fScale * abs..
    bool and = (ul == 0) || (um == 0) || (ur == 0) || (ml == 0) || (mr == 0) || (ll == 0) || (lm == 0) || (lr == 0);
	Sum = __max(0, Sum);
	Sum = __min(0xFF, Sum);
	Sum = (Sum != 0 && mm != valm && and) ? 254 : 0;
    return (unsigned char) Sum;
}
__global__ void silhouetteTex( unsigned char *psOriginal, unsigned int Pitch, int w, int h, float fScale )
{ 
	int i;
	unsigned char val;
	unsigned char *pSihluette = psOriginal + blockIdx.x*Pitch;
    for (i = threadIdx.x; i < w; i += blockDim.x) 
	{
        unsigned char pix00 = tex2D(texs, (float) i-1, (float) blockIdx.x-1);
        unsigned char pix01 = tex2D(texs, (float) i+0, (float) blockIdx.x-1);
		unsigned char pix02 = tex2D(texs, (float) i+1, (float) blockIdx.x-1);
		unsigned char pix10 = tex2D(texs, (float) i-1, (float) blockIdx.x+0);
		unsigned char pix11 = tex2D(texs, (float) i+0, (float) blockIdx.x+0);
		unsigned char pix12 = tex2D(texs, (float) i+1, (float) blockIdx.x+0);
		unsigned char pix20 = tex2D(texs, (float) i-1, (float) blockIdx.x+1);
		unsigned char pix21 = tex2D(texs, (float) i+0, (float) blockIdx.x+1);
		unsigned char pix22 = tex2D(texs, (float) i+1, (float) blockIdx.x+1);
		val = computeSilhouettePP(pix00, pix01, pix02, pix10, pix11, pix12, pix20, pix21, pix22, 0, 0, fScale);
		pSihluette[i] = val;
	}
}

__global__ void sTex( unsigned char *psOriginal, unsigned int Pitch, int w, int h, float fScale )
{ 
	int i;
	unsigned char pix00, pix01, pix02, pix10, pix11, pix12, pix20, pix21, pix22;
	unsigned char pixMax1, pixMax2, pixMax3, pixMax4;
	unsigned char *ps = psOriginal + blockIdx.x*Pitch;
    for (i = threadIdx.x; i < w; i += blockDim.x) 
	{
        pix00 = tex2D(texs, (float) i-1, (float) blockIdx.x-1);
        pix01 = tex2D(texs, (float) i+0, (float) blockIdx.x-1);
        pix02 = tex2D(texs, (float) i+1, (float) blockIdx.x-1);
        pix10 = tex2D(texs, (float) i-1, (float) blockIdx.x+0);
        pix11 = tex2D(texs, (float) i+0, (float) blockIdx.x+0);
        pix12 = tex2D(texs, (float) i+1, (float) blockIdx.x+0);
        pix20 = tex2D(texs, (float) i-1, (float) blockIdx.x+1);
        pix21 = tex2D(texs, (float) i+0, (float) blockIdx.x+1);
        pix22 = tex2D(texs, (float) i+1, (float) blockIdx.x+1);
		pixMax1 = __max(pix00, pix01);
		pixMax2 = __max(pix02, pix10);
		pixMax3 = __max(pix12, pix20);
		pixMax4 = __max(pix21, pix22);
		pixMax1 = __max(pixMax1, pixMax3);
		pixMax2 = __max(pixMax2, pixMax4);
		pixMax1 = __max(pixMax1, pixMax2);
		ps[i] = computesPP(pix00, pix01, pix02, pix10, pix11, pix12, pix20, pix21, pix22, pixMax1, 0, fScale);
    }
}

__host__ void computes(unsigned char *originalImage, unsigned char *sImage, int w, int h, float fScale)
{
	shape_inferenceSafeCall(cudaMemcpy2DToArray(data->arrays, 0, 0, originalImage, w, w, h, cudaMemcpyDeviceToDevice));
	shape_inferenceSafeCall(cudaBindTextureToArray(texs, data->arrays));
	shape_inferenceSafeCall(cudaMemset(sImage, 0, sizeof(unsigned char)*w*h));
	sTex<<<h, 384>>>(sImage, w, w, h, fScale);
	shape_inferenceSafeCall(cudaUnbindTexture(texs));
}
__host__ void computeSihluette(unsigned char *originalImage, unsigned char *sihlutteImage, int w, int h, float fScale)
{
	shape_inferenceSafeCall(cudaMemcpy2DToArray(data->arrays, 0, 0, originalImage, w, w, h, cudaMemcpyDeviceToDevice));
	shape_inferenceSafeCall(cudaBindTextureToArray(texs, data->arrays));
	shape_inferenceSafeCall(cudaMemset(sihlutteImage, 0, sizeof(unsigned char)*w*h));
	silhouetteTex<<<h, 384>>>(sihlutteImage, w, w, h, fScale);
	shape_inferenceSafeCall(cudaUnbindTexture(texs));
}