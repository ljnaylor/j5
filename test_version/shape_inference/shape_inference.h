#pragma once

#include "Objects/Object3D.h"
#include "Objects/View3D.h"
#include "Optimiser/Engine/OptimisationEngine.h"
#include "Utils/HistogramEngine.h"
#include "Utils/ImageUtils.h"
#include "Utils/VisualisationEngine.h"

using namespace shape_inference::Objects;
using namespace shape_inference::Optimiser;
using namespace shape_inference::Utils;
