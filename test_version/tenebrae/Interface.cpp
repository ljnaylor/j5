#include "../shape_inference/shape_inference.h"

#include "Utils/Timer.h"
/*#include <C:\Users\User\Desktop\opencv\build\include\opencv2\opencv.hpp>*/
#include <C:\opencv\build\include\opencv2\opencv.hpp>

using namespace shape_inference::Utils;

int main(void)
{
	// red box demo
	//std::string sModelPath = "../Files/Models/Renderer/long.obj";

	//pocketwatch
	//std::string sModelPath = "../Files/Models/Renderer/basic/pocketwatch.obj";
	//std::string sSrcImage = "../Files/Images/basic/pocketwatch.png";
	//std::string sCameraMatrix = "../Files/CameraCalibration/Kinect.cal";
	//std::string sTargetMask = "../Files/Masks/480p_All_VideoMask.png";
	//std::string sHistSrc = "../Files/Masks/pocketwatch.png";
	//std::string sHistMask = "../Files/Masks/segmasks/pocketwatchMask.png";


	//tape
	//std::string sModelPath = "../Files/Models/Renderer/basic/tape.obj";
	//std::string sSrcImage = "../Files/Images/basic/tape.png";
	//std::string sCameraMatrix = "../Files/CameraCalibration/Kinect.cal";
	//std::string sTargetMask = "../Files/Masks/480p_All_VideoMask.png";
	//std::string sHistSrc = "../Files/Masks/tape.png";
	//std::string sHistMask = "../Files/Masks/segmasks/tapemask.png";


	//l
	//std::string sModelPath = "../Files/Models/Renderer/basic/l.obj";
	//std::string sSrcImage = "../Files/Images/basic/l.png";
	//std::string sCameraMatrix = "../Files/CameraCalibration/Kinect.cal";
	//std::string sTargetMask = "../Files/Masks/480p_All_VideoMask.png";
	//std::string sHistSrc = "../Files/Masks/l.png";
	//std::string sHistMask = "../Files/Masks/segmasks/lmask.png";

	//key
	//std::string sModelPath = "../Files/Models/Renderer/basic/key.obj";
	//std::string sSrcImage = "../Files/Images/basic/key.png";
	//std::string sCameraMatrix = "../Files/CameraCalibration/Kinect.cal";
	//std::string sTargetMask = "../Files/Masks/480p_All_VideoMask.png";
	//std::string sHistSrc = "../Files/Masks/key.png";
	//std::string sHistMask = "../Files/Masks/segmasks/keymask.png";

	//tube
	std::string sModelPath = "../Files/Models/Renderer/basic/tube.obj";
	std::string sSrcImage = "../Files/Images/basic/tube.png";
	std::string sCameraMatrix = "../Files/CameraCalibration/Kinect.cal";
	std::string sTargetMask = "../Files/Masks/480p_All_VideoMask.png";
	std::string sHistSrc = "../Files/Masks/tube.png";
	std::string sHistMask = "../Files/Masks/segmasks/tubemask.png";

	//L
	//std::string sModelPath = "../Files/Models/Renderer/basic/l.obj";
	//std::string sSrcImage = "../Files/Images/basic/L.png";
	//std::string sCameraMatrix = "../Files/CameraCalibration/Kinect.cal";
	//std::string sTargetMask = "../Files/Masks/480p_All_VideoMask.png";
	//std::string sHistSrc = "../Files/Masks/L.png";
	//std::string sHistMask = "../Files/Masks/segmasks/L.png";


	// ---------------------------------------------------------------------------
	char str[100];
	int i;

	int width = 640, height = 480;
	int viewCount = 1, objectCount = 1;
	int objectId = 0, viewIdx = 0, objectIdx = 0;

	Timer t;

	//result visualisation
	ImageUChar4* ResultImage = new ImageUChar4(width, height);

	// ---------------------------------------------------------------------------
	//input image
	//camera = 24 bit colour rgb
	ImageUChar4* camera = new ImageUChar4(width, height);
	ImageUtils::Instance()->LoadImageFromFile(camera, (char*)sSrcImage.c_str());
	ImageUtils::Instance()->LoadImageFromFile(camera, (char*)sCameraMatrix.c_str());

	//objects allocation + initialisation: 3d model in obj required
	Object3D **objects = new Object3D*[objectCount];

	std::cout << "\n==[APP] Init Model ==" << std::endl;
	objects[objectIdx] = new Object3D(objectId, viewCount, (char*)sModelPath.c_str(), width, height);

	// ---------------------------------------------------------------------------
	//views allocation + initialisation: camera calibration (artoolkit format) required
	std::cout << "\n==[APP] Init CameraMatrix ==" << std::endl;
	View3D **views = new View3D*[viewCount];
	views[viewIdx] = new View3D(0, (char*)sCameraMatrix.c_str(), width, height);


	// ---------------------------------------------------------------------------
	//histogram initialisation
	//source = 24 bit colour rgb
	// mask = 24 bit black/white png - white represents object
	//videoMask = 24 bit black/white png - white represents parts of the image that are usable
	std::cout << "\n==[APP] Init Target ROI ==" << std::endl;
	ImageUtils::Instance()->LoadImageFromFile(views[viewIdx]->videoMask,
		(char*)sTargetMask.c_str());

	ImageUtils::Instance()->LoadImageFromFile(objects[objectIdx]->histSources[viewIdx],
		(char*)sHistSrc.c_str());

	ImageUtils::Instance()->LoadImageFromFile(objects[objectIdx]->histMasks[viewIdx],
		(char*)sHistMask.c_str(), objectIdx + 1);

	HistogramEngine::Instance()->UpdateVarBinHistogram(
		objects[objectIdx], views[viewIdx], objects[objectIdx]->histSources[viewIdx],
		objects[objectIdx]->histMasks[viewIdx], views[viewIdx]->videoMask);


	// ---------------------------------------------------------------------------
	//iteration configuration for one object
	IterationConfiguration *iterConfig = new IterationConfiguration();
	iterConfig->width = width; iterConfig->height = height;
	iterConfig->iterViewIds[viewIdx] = 0;
	iterConfig->iterObjectCount[viewIdx] = 1;
	iterConfig->levelSetBandSize = 8;
	iterConfig->iterObjectIds[viewIdx][objectIdx] = 0;
	iterConfig->iterViewCount = 1;
	iterConfig->iterCount = 50;

	//step size per object and view
	objects[objectIdx]->stepSize[viewIdx] = new StepSize3D(0.2f, 0.5f, 0.5f, 10.0f);

	//initial pose per object and view
	// Notice the input pose here is angle, not radians for the rotation part

	//casette objects[objectIdx]->initialPose[viewIdx]->SetFrom(2.0f, 0.0f, 5.f, 240.f, 90.f, 0.f);

	//tube objects[objectIdx]->initialPose[viewIdx]->SetFrom(-0.5f, -1.8f, 6.f, 30.f, 90.f, 30.f);

	//tape
	//objects[objectIdx]->initialPose[viewIdx]->SetFrom(0.0f, 0.0f, 9.f, -160.f, 45.f, 20.f);
	//l
	objects[objectIdx]->initialPose[viewIdx]->SetFrom(0.f, 0.f, 12.f, 0.f, 60.f, 20.f);

	//objects[objectIdx]->initialPose[viewIdx]->SetFrom(-1.98f, -2.90f, 37.47f, -40.90f, -207.77f, 27.48f);
	//objects[objectIdx]->initialPose[viewIdx]->SetFrom(-1.98f, -2.90f, -10.f, -40.90f, -100.77f, 27.48f);
	//objects[objectIdx]->initialPose[viewIdx]->SetFrom(3.98f, -1.90f, -60.47f, -0.90f, -80.77f, 15.48f);
	//objects[objectIdx]->initialPose[viewIdx]->SetFrom(5.f, -2.f, 15.f, -10.f, -200.f, 30.f);
	//for blue car demo
	//objects[objectIdx]->initialPose[viewIdx]->SetFrom( -3.0f,-4.5f, 100.f, -250.90f, -180.77f, 87.48f);

	//primary initilisation
	OptimisationEngine::Instance()->Initialise(width, height);

	//register camera image with main engine
	OptimisationEngine::Instance()->RegisterViewImage(views[viewIdx], camera);

	// ---------------------------------------------------------------------------
	std::cout << "\n==[APP] Rendering object initial pose.. ==" << std::endl;
	VisualisationEngine::Instance()->GetImage(
		ResultImage, GETIMAGE_PROXIMITY,
		objects[objectIdx], views[viewIdx],
		objects[objectIdx]->initialPose[viewIdx]);

	cv::Mat ResultMat(height, width, CV_8UC4, ResultImage->pixels);
	cv::imshow("initial pose", ResultMat);
	cv::waitKey(1000);

	std::cout << "[App] Finish Rendered object initial pose." << std::endl;

	for (i = 0; i < 4; i++)
	{
		switch (i)
		{
		case 0:
			iterConfig->useCUDAEF = true;
			iterConfig->useCUDARender = true;
			break;
		case 1:
			iterConfig->useCUDAEF = false;
			iterConfig->useCUDARender = true;
			break;
		case 2:
			iterConfig->useCUDAEF = true;
			iterConfig->useCUDARender = false;
			break;
		case 3:
			iterConfig->useCUDAEF = false;
			iterConfig->useCUDARender = false;
			break;
		}

		printf("======= mode: useCUDAAEF: %d, use CUDARender %d ========;\n",
			iterConfig->useCUDAEF, iterConfig->useCUDARender);

		sprintf(str, "result%04d.png", i);

		//main processing
		t.restart();
		OptimisationEngine::Instance()->Minimise(objects, views, iterConfig);
		t.check("Iteration");

		//result plot
		VisualisationEngine::Instance()->GetImage(
			ResultImage, GETIMAGE_PROXIMITY,
			objects[objectIdx], views[viewIdx], objects[objectIdx]->pose[viewIdx]);

		//result save to file
		//    ImageUtils::Instance()->SaveImageToFile(result, str);
		cv::Mat ResultMat(height, width, CV_8UC4, ResultImage->pixels);
		cv::imshow("result", ResultMat);
		cv::waitKey(5000);

		printf("final pose result %f %f %f %f %f %f %f\n\n",
			objects[objectIdx]->pose[viewIdx]->translation->x,
			objects[objectIdx]->pose[viewIdx]->translation->y,
			objects[objectIdx]->pose[viewIdx]->translation->z,
			objects[objectIdx]->pose[viewIdx]->rotation->vector4d.x,
			objects[objectIdx]->pose[viewIdx]->rotation->vector4d.y,
			objects[objectIdx]->pose[viewIdx]->rotation->vector4d.z,
			objects[objectIdx]->pose[viewIdx]->rotation->vector4d.w);
	}

	//posteriors plot
	sprintf(str, "posteriors.png");
	VisualisationEngine::Instance()->GetImage(
		ResultImage, GETIMAGE_POSTERIORS,
		objects[objectIdx], views[viewIdx], objects[objectIdx]->pose[viewIdx]);

	ImageUtils::Instance()->SaveImageToFile(ResultImage, str);

	//primary engine destructor
	//OptimisationEngine::Instance()->Shutdown();

	//for (i = 0; i < objectCount; i++) delete objects[i];
	//delete objects;

	//for (i = 0; i < viewCount; i++) delete views[i];
	//delete views;

	//delete ResultImage;

	//std::cout << "Exit tenebrae app successfully." << std::endl;

	//return 0;

}
